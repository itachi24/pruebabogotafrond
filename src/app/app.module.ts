import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
import { EmployeeComponent } from './employee/employee.component';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeService } from './employee/employee.service';
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './employee/form.component';

registerLocaleData(localeES, 'es');

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'employee' },
  { path: 'employees', component: EmployeeComponent },
  { path: 'employees/form', component: FormComponent },
  { path: 'employees/form/:idemployee', component: FormComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    EmployeeService,
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
