import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmployeeService } from './employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public employee: Employee = new Employee();
  titulo: string = "Crear usuario";
  login_usuario1: string;

  constructor(private employeeService: EmployeeService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarUsuario();
  }

  cargarUsuario(): void {
    // console.log("entra a cargarUsuario");
    this.activatedRoute.params.subscribe(params => {
      let login_usuario = params['idemployee']
      // console.log("entra a cargarUsuario: " + params.login_usuario);
      if (login_usuario) {
        this.titulo = "Actualizar usuario";
        this.employeeService.getEmployee(login_usuario).subscribe(
            (employee) => {
              this.employee = employee
              // ,
              //    console.log("usuario= " + this.usuario.passwordf)
            }
          );
        }
    })
  }

  public create(): void {
    // console.log("login_usuario1= " + this.login_usuario1);
    this.employee.idemployee = +this.login_usuario1;
    this.employeeService.create(this.employee).subscribe(
      response => this.router.navigate(['/employees'])
    );
    swal.fire('Nuevo usuario', `El usuario:  ${this.login_usuario1} ha sido creado con éxito`, 'success');
  }

  public update(): void {
    this.employeeService.update(this.employee).subscribe(
      response => this.router.navigate(['/employees'])
    );
    swal.fire('Usuario actualizado', `El usuario:  ${this.employee.idemployee} ha sido modificado con éxito`, 'success');
  }

}
