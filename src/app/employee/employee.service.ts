import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee';
import { map, catchError, tap } from 'rxjs/operators';
import swal from 'sweetalert2';

@Injectable()

export class EmployeeService {
    private urlEndPoint: string = 'http://localhost:8090/demo-0.0.1-SNAPSHOT/api/employees';
    constructor(private http: HttpClient) { }

    getEmployees(): Observable<Employee[]> {
        return this.http.get(this.urlEndPoint).pipe(map(response => response as Employee[]));
    }

    create(Employee: Employee): Observable<any> {
        return this.http.post<any>(this.urlEndPoint, Employee);
    }

    getEmployee(idemployee: number): Observable<Employee> {
        return this.http.get<Employee>(this.urlEndPoint + '/' + idemployee).pipe( /*, { headers: this.agregarAuthorizationHeader() }*/
            catchError(e => {

                if (e.status == 400) {
                    return throwError(e);
                }
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    update(Employee: Employee): Observable<any> {
        return this.http.put<any>(`${this.urlEndPoint}/${Employee.idemployee}`, Employee).pipe( /*, { headers: this.agregarAuthorizationHeader() }*/
            catchError(e => {

                if (e.status == 400) {
                    return throwError(e);
                }
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    delete(idemployee: number): Observable<any> {
        return this.http.delete(`${this.urlEndPoint}/${idemployee}`).pipe(
            catchError(e => {

                if (e.status == 500) {
                    swal.fire('Error al eliminar', e.error.mensaje + '\n' + e.error.error, 'error')
                    return throwError(e);
                }

                if (e.status == 400) {
                    return throwError(e);
                }

                if (e.error.mensaje) {
                    // console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    getPageEmployees(page: number): Observable<any> {
        return this.http.get(this.urlEndPoint + '/page/' + page).pipe(
            tap((response: any) => {
                // console.log('EmployeeService: tap 1');
                (response.content as Employee[])
                // .forEach(Employee => console.log(Employee.idemployee));
            }),
            map((response: any) => {
                (response.content as Employee[]).map(Employee => {
                    return Employee;
                });
                return response;
            }),
            tap(response => {
                // console.log('EmployeeService: tap 2');
                (response.content as Employee[])
                // .forEach(Employee => console.log(Employee.idemployee)
                // );
            })
        );
    }

}
