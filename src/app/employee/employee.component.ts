import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmployeeService } from './employee.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees: Employee[];


  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.getEmployees().subscribe(
      employees => this.employees = employees
    );
  }

  delete(employee: Employee): void {

    swal.fire({
        title: 'Está seguro?',
        text: `¿Seguro que desea eliminar al usuario ${employee.idemployee} ?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No, cancelar!'
    }).then((result) => {
        if (result.value) {

            this.employeeService.delete(employee.idemployee).subscribe(
                () => {
                    this.employees = this.employees.filter(cli => cli !== employee)
                    swal.fire(
                        'Usuario Eliminado!',
                        `Usuario ${employee.idemployee} eliminado con éxito.`,
                        'success'
                    )
                },
            )
        }
    });
}

}
